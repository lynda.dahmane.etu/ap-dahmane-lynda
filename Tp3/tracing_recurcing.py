#Dahmane lynda
#TP3
#31/01/2024

from ap_decorators import trace
@trace
def somme(a:int,b:int)->int:
    """renvoie la somme des deux entiers a et b

    Précondition : 
    Exemple(s) :
    $$$ somme(4,5)
    9
    $$$ somme(2,1)
    3

    """
    if a==0:
        res=b
    else:
        res=somme(a-1,b+1)
    return res

@trace
def binomial(n:int,k:int)->int:
    """renvoie le coefficient binomial ,qui represente le nombre de façon
de choisir k elements parmis un ensemble de n elements.

    Précondition : 
    Exemple(s) :
    $$$ binomial(7,3)
    35

    """
    if k==0 or k==n:
        return 1
    else:
        return binomial(n-1,k-1)+binomial(n-1,k)
    
       
@trace  
def is_palindromic(mot:str)->bool:
    """renvoie True si le mot est un palindromic sinon renvoie False

    Précondition : 
    Exemple(s) :
    $$$ is_palindromic("radar")
    True
    $$$ is_palindromic("hello")
    False

    """
    
    if len(mot)<=1:
        return True
    elif mot[0]==mot[-1]:
        return is_palindromic(mot[1:-1])
    else:
        return False
    


