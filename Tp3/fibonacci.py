#Q1:
# on calcul les nombres de fibonacci jusqu'à n=10:
#f_0=0 etf_1=1
#f_2=f_1+f_0=1+0=1....f_9=f_8+f_7=21+13=24, et f_10=f_9+f_8=34+21=55

from ap_decorators import count
@count
def fibo(n:int)->int:
    """renvoie les termes de la suite Fibonacci

    Précondition : 
    Exemple(s) :
    $$$ fibo(10)
    55
    $$$ fibo(8)
    21
    $$$ fibo(1)
    1

    """
    if n<=1:
        return n
    else:
        return fibo (n-1)+fibo(n-2)
    
#fibo(40) donne : 102334155 , que f_40 est assez grand , donc les nombres de la suite fibonacci
#peuvent devenir grands à mesure que n augmente.
    

    
    
    