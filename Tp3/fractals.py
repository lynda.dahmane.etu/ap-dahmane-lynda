import turtle


def zigzag():
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    
    for i in range (5):
        
        turtle.left(60)
        turtle.forward(50)
        turtle.right(120)
        turtle.forward(50)
        turtle.left(120)
        turtle.forward(50)
        turtle.right(120)
        turtle.forward(50)
        turtle.left(60)
        
#Q1:
        


def von_koch(l:int|float,n:int)->int:
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    if n==0:
        turtle.forward(l)
    else:
        von_koch(1/3,n-1)
        turtle.left(60)
        von_koch(1/3,n-1)
        turtle.right(120)
        von_koch(1/3,n-1)
        turtle.left(60)
        von_koch(1/3,n-1)
         
        
        
        

def von_koch2(longueur:int|float,n:int)->int:
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    if n==0:
        turtle.forward(longueur)
    else:
        von_koch(1/3,n-1)
        turtle.left(60)
        von_koch(1/3,n-1)
        turtle.right(120)
        von_koch(1/3,n-1)
        turtle.left(60)
        von_koch(1/3,n-1)
        
        
        
#Q2:
#la relation est que à chaque fois que l'ordre augmente de 1, la longueur de tracé est réduite de loitié.
#Cela signifie que plus l'ordre est élevé, plus le tracé devient détaillé et complexe, mais la largeur occupée
#par le tracé reste relativement constante.
        



def courbe_cesaro(l:int|float,n:int)->int:
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    if n==0:
        turtle.forward(l)
    else:
        courbe_cesaro(1/3,n-1)
        turtle.left(85)
        courbe_cesaro(1/3,n-1)
        turtle.right(170)
        courbe_cesaro(1/3,n-1)
        turtle.left(85)
        courbe_cesaro(1/3,n-1)
         
        