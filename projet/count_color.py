from PIL import Image

def count_color(image, color:tuple([int, int,int]))-> int:
    """renvoie le nombre de pixels d'une certaine couleur dans une image donnée 

    Précondition : 
    Exemple(s) :
    $$$ count_color(im, (255, 0, 0)) 
    100
    $$$ count_color(im, (0, 255, 0))   
    50
    $$$ count_color(im, (0, 0, 255)) 
    200

    """
    im_rgb = image.convert('RGB')
   
    count = 0
    width, height = im_rgb.size
    for x in range(width):
        for y in range(height):
            if im_rgb.getpixel((x, y)) == color:
                count += 1

    return count

