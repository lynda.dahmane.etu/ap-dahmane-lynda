from PIL import Image

def diviser_image(image, taille:int):
    """renvoie une liste de blocs d'images. Chaque bloc est une partie
    de l'image initiale, découpée en carrés de taille spécifiée

    """
    
    largeur, hauteur = image.size
    blocs = []
    
    for i in range(0, largeur, taille):
        for j in range(0, hauteur, taille):
            bloc = image.crop((i, j, i+taille, j+taille))
            blocs.append(bloc)
    
    return blocs