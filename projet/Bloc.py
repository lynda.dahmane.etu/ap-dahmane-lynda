

class Block:
   
   
   
    def __init__(self, zone, couleur=None, sous_blocs=None):
        """Initialisation un objet Bloc avec une zone, une couleur et des sous_blocs"""
        
        self.zone=zone
        self.couleur=couleur
        self.sous_blocs=sous_blocs
        
                    
    def moyenne_couleurs(self):
        """Renvoie la moyenne des couleurs des sous_blocs"""

        if not self.sous_blocs:
            return  self.couleur
        else:
            r,g,b=0, 0, 0
            for blocs in self.sous_blocs:
                r+=bloc.couleur[0]
                g+=bloc.couleur[1]
                b+=bloc.couleur[2]
            return [r/len(self.sous_blocs), g / len(self.sous_blocs),b/len(self.sous_blocs)]
        
        
        
        
    def est_uniforme(self)->bool:
        """Renvoie True si le bloc est uniforme"""
        return not self.sous_blocs
    
    
    
    def est_dans_limites(self,limites_min, limites_max):
       """Renvoie True si la zone du bloc est dans les limites spécifiées """
       x,y=self.zone[:2]
       width, height=self.zone[2:]
       return (x>=limites_min[0] and x + width<=limites_max[0] and y>=limites_min[1] and x + height<=limites_max[1])
   
   
   
   
   
   
    def __str__(self):
        
        """Renvoie une représentation en chaine de caractére du bloc"""
        
        if self.est_uniforme():
            return f"Bloc(uniforme, couleur={self.couleur})"
        else:
            return f"Bloc(non_uniforme, couleur={self.couleur})"
            
        
        
        
        
        
        

                
    
               
     
      
      
    
    
    
    

            
            
    