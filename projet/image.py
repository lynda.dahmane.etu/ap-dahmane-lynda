def couleur_moyenne(liste_couleurs:[tuple])->[tuple]:
    """calcule la couleur moyenne à partir d'une liste de couleurs. Elle prend en entrée
    une liste de tuples représentant des couleurs et retourne un tuple représentant la couleur moyenne

    Précondition : 
    Exemple(s) :
    $$$ couleur_moyenne([(255, 0, 0), (0, 255, 0), (0, 0, 255)])
    (85, 85, 85)
    """
    
    total_rouge = 0
    total_vert = 0
    total_bleu = 0
    
    for couleur in liste_couleurs:
        total_rouge += couleur[0]
        total_vert += couleur[1]
        total_bleu += couleur[2]
    
    nb_couleurs = len(liste_couleurs)
    moyenne_rouge = total_rouge / nb_couleurs
    moyenne_vert = total_vert / nb_couleurs
    moyenne_bleu = total_bleu / nb_couleurs
    
    return (moyenne_rouge, moyenne_vert, moyenne_bleu)

