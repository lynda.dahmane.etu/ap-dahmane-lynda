#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:author: FIL - FST - Univ. Lille.fr <http://portail.fil.univ-lille.fr>_
:date: janvier 2019
:last revised: 
:Fournit :
"""
from date import Date


class Etudiant:
    """
    une classe représentant des étudiants.

    $$$ etu = Etudiant(314159, 'Oléon', 'Tim', Date(7,11,2004), 'MI', '15')
    $$$ str(etu)
    'Tim Oléon'
    $$$ repr(etu)
    '314159 : Tim OLÉON'
    $$$ etu.prenom
    'Tim'
    $$$ etu.nip
    314159
    $$$ etu.nom
    'Oléon'
    $$$ etu.formation
    'MI'
    $$$ etu.groupe
    '15'
    $$$ etu2 = Etudiant(314159, 'Oléon', 'Tim', Date(7,11,2004), 'MI', '15')
    $$$ etu == etu2
    True
    $$$ etu3 = Etudiant(141442, 'Oléon', 'Tim', Date(7,11,2004), 'MI', '15')
    $$$ etu == etu3
    False
    $$$ etu4 = Etudiant(141442, 'Calbuth', 'Raymond', Date(2,1,2005), 'MI', '11')
    $$$ etu < etu4
    True
    $$$ isinstance(etu.naissance, Date)
    True
    """
    
    
    
    def __init__(self, nip: int, nom: str, prenom: str,
                 naissance: Date, formation: str, groupe: str):
        """
        initialis-(e un nouvel étudiant à partir de son nip, son nom, son
        prénom, sa formation et son groupe.

        précondition : le nip, le nom et le prénom ne peuvent être nuls ou vides.
        """
        self.prenom=prenom
        self.nom=nom
        self.nip=nip
        self.naissance=naissance
        self.formation=formation
        self.groupe=groupe

    def __eq__(self, other) -> bool:
        """
        Renvoie True ssi other est un étudiant ayant :
        - même nip,
        - même nom et
        - même prénom que `self`,
        et False sinon.
        """
        if self.nip==other.nip and self.nom==other.nom and self.prenom==other.prenom:
            return True
        return False
    
       
    def __lt__(self, other) -> bool:
        """
        Renvoie True si self est né avant other
        """
    
        return self.naissance < other.naissance
        
        
        
    def __str__(self) -> str:
        """
        Renvoie une représentation textuelle de self.
        """
        return f"{self.prenom} {self.nom}"

    def __repr__(self) -> str:
        """
        Renvoie une représentation textuelle interne de self pour le shell.
        """
        x=self.nom.upper()
        return f"{self.nip} : {self.prenom} {x}"

if (__name__ == "__main__"):
    import apl1test
    apl1test.testmod('etudiant.py')
    
    
def charge_fichier_etudiants(fname: str) -> list[Etudiant]:
    """
    Renvoie la liste des étudiants présents dans le fichier dont
    le nom est donné en paramètre.

    précondition: le fichier est du bon format.
    """
    res = []
    with open(fname, 'r') as fin:
        fin.readline()
        ligne = fin.readline()
        while ligne != '':
            nip, nom, prenom, naissance, formation, groupe = ligne.strip().split(';')
            y, m, d = naissance.split('-')
            date_naiss = Date(int(d.lstrip('0')), int(m.lstrip('0')), int(y))
            res.append(Etudiant(nip, nom, prenom, date_naiss, formation, groupe))
            ligne = fin.readline()
    return res

L_ETUDIANTS=charge_fichier_etudiants("etudiants.csv")
COURTE_LISTE=L_ETUDIANTS[:10]

def est_liste_d_etudiants(x) -> bool:
    """
    Renvoie True si ``x`` est une liste de d'étudiant, False dans le cas contraire.

    Précondition: aucune

    Exemples:

    $$$ est_liste_d_etudiants(COURTE_LISTE)
    True
    $$$ est_liste_d_etudiants("Timoleon")
    False
    $$$ est_liste_d_etudiants([('12345678', 'Calbuth', 'Raymond', 'Danse', '12') ])
    False
    """

    if isinstance(x,list):
        for element in x:
            if not isinstance(element,Etudiant):
                return False
        return True
    return False
#Q4:

NBRE_ETUDIANTS=len(L_ETUDIANTS)

#Q5:
NIP=42320929



#Q6:
def ensemble_des_formations(liste: list[Etudiant]) -> Set[str]:
    """
    Renvoie un ensemble de chaînes de caractères donnant les formations
    présentes dans les fiches d'étudiants

    Précondition: liste ne contient que des fiches d'étudiants

    Exemples:

    $$$ ensemble_des_formations(COURTE_LISTE)
    {'MI', 'PEIP', 'MIASHS', 'LICAM'}
    $$$ ensemble_des_formations(COURTE_LISTE[0:2])
    {'MI', 'MIASHS'}
    """
    res=[]
    
    for etudiant in liste:
        res.add(etudiant.formation)
    return res
        
                        