#nom:
#prenom:
#groupe:
#date:


def pour_tous(seq_bool: list[bool]) -> bool:
    """
    Renvoie True ssi `seq_bool` ne contient pas False

    Exemples:

    $$$ pour_tous([])
    True
    $$$ pour_tous((True, True, True))
    True
    $$$ pour_tous((True, False, True))
    False
    """

    for element in seq_bool:
        if not element:
            return False
    return True
      
            
            
def il_existe(seq_bool: list[bool]) -> bool:
    """

    Renvoie True si seq_bool contient au moins une valeur True, False sinon

    Exemples:

    $$$ il_existe([])
    False
    $$$ il_existe([False, True, False])
    True
    $$$ il_existe([False, False])
    False
    """
    for element in seq_bool:
        if element:
            return True
    return False

                 
            
            
            
            
            
            
            
        
        
    
    
    
    
    
    
    
    
    
    
    





















    
    





















































































































































































































































