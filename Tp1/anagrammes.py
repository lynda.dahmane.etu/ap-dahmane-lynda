#Methodesplit

#Question 1

# s='la méthode split est parfois bien utile'
# >>>s.split(' ')
# ['la','methode','split','est','parfois','bien','utile']
# >>>s.split('e')
# ['la method','split','st parfois bi','n util']
# >>>s.split('é')
# ['la m','thode split','est','parfois','bien','utile']
# >>>s.split()
# ['la' ,'méthode' ,'split' ,'est' ,'parfois' ,'bien' ,'utile']
# 
#Question 2:
# la methode split est utiliseé pour diviser une chaine en une liste de sous-chaine, en utulisant un séparateur spécifié.
# Cette methode elle utilise l'espace comme le séparateur.
#Question 3:
# Non la methode elle modifie pas la chaine à la quelle s'applique , elle renvoie une nouvelleliste de sous chaine résultant de la
#dévision de la chaine original, c'est à dire que la chaine original elle reste inchangée.

#Methode join
#s="la méthode split est parfois bien utile"
#>>>"".join(l)
#("la methode split est bein util")
#>>>"".join(l)
#(lamethodesplitestbienutile)
#>>>";".join(l)
#("la;methode;split;est;bein;utile")
#>>>" tralala ".join(l)
#("la méthode split est parfois bien utile")
#>>>print ("\n".join(l))
#"la méthode split est parfois bien utile"
#>>>"".join(s)
#"la méthode split est parfois bien utile"
#>>>"!".join(s)
#"la méthode split est parfois bien utile"
#>>>"".join()
#(" ")
#>>>"".join([])
#[]
#>>>"".join([1,2])
#"12"

#Question 2:
# La methode join est utiliseé pour joindre les éléments d'une liste en une suele chaine de caractères
#Elle prend une chaine de caractère en argument et insère cette chaine entre chaque elément de la liste 

#Question 3:
# La methode join elle modifie pas la chaine à laquelle elle est appliqueé. Elle renvoie plutot une nouvelle
#chaine qui est le résultat de la jonction des éléments de la liste avec la chaine specifie.Donc la chaine
#origine rete inchangée.

#Question 4:
# 
def join (chaine:str,listchaine:list[str])->str:
    """renvoie une nouvelle chaine de caractéres en concaténant les élement de
    l en les intercalant avec s.

crypto#     Précondition : 
    Exemple(s) :
    $$$ join('.', ['raymond', 'calbuth', 'ronchin', 'fr'])
    'raymond.calbuth.ronchin.fr'


    """
    res= ""
    for i in range(len(listchaine)-1):
         res+=listchaine[i]+chaine
    res+=listchaine[-1]
    return res

#Methode sort

#Question1:
#l=list('timoleon')
#['e','i','l','m','n','o','o','t']
#l=lists
#s="Je n'ai jamais joué de flûte."
#['',"'",'','','','','','','','','','','','','','','','','','','','','','',''','','','']
#Question 2:
#si on applique cette methode sur la liste l=['a', 1] , la methode sort générera une erreur
#car les types de données doivent etre cohérents pour pouvoir etre triés.

# Une fonction sort pour les chaines:
def sort(chaine:str)->str:
    """"Renvoie une chaîne de caractères contenant les caractères de `s` triés dans l'ordre croissant

    Précondition : aucune
    Exemple(s) :
    $$$ sort('orange')
    'aegnor'


    """
    
    list=[]
    res=''
    alpha=['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
    for i in range (len(chaine)):
        for i in range(len(alpha)):
            if alpha[i]==chaine[i]:
                list.append(alpha[i])
    return res

#Anagrammes:
#Question 1:
def sont_anagrammes(s1: str, s2: str) -> bool:
    """ Renvoie True si s1 et s2 sont anagrammatiques, False sinon.

    Précondition : aucune
    Exemple(s) :
    $$$ sont_anagrammes('orange', 'organe')
    True
    $$$ sont_anagrammes('orange','Organe')
    False
    """

    l1,l2=list(s1),list(s2)
    l1.sort()
    l2.sort()
    return l1==l2

# #Question 2:
def sont_anagrammes(s1: str, s2: str) -> bool:
    """Renvoie True si s1 et s2 sont anagrammatiques, False sinon

    Précondition : aucune
    Exemple(s) :
    $$$ sont_anagrammes('orange', 'organe')
    True
    $$$ sont_anagrammes('orange','Organe')
    False
    """

    dict1=dict()
    dict2=dict()
    for elt in s1:
        if elt in dict1:
            dect1[elt]=dict1[elt]+1
        else:
            dict1[elt]=1
    
    for elt in s2:
        if elt in dict2:
            dect2[elt]=dict2[elt]+1
        else:
            dict2[elt]=1
    d1,d2=list(dict1),list(dict2)
    d1.sort(),d2.sort()
    return d1==d2

    
#  #question 3:
# 
def sont_anagrammes(s1: str, s2: str) -> bool:
    """Renvoie True si s1 et s2 sont anagrammatiques, False sinon

    Précondition : aucune
    Exemple(s) :
    $$$ sont_anagrammes('orange', 'organe')
    True
    $$$ sont_anagrammes('orange','Organe')
    False
    $$$ sont_anagrammes('orange','OrganE')
    False
    """

    #verifier si les 2 chaines ont le memes caracteres et la meme longueur
    if len(s1)!=len(s2):
        return False
    #verifier si les 2 chaines ont le memes caracteres et le meme nombre d'occurrences
    for i in range(len(s1)):
        if s1.count(s1[i])!=s2.count(s2[i]):
            return False
    #si toutes les conditions sont remplies, les chaines sont anagrammatiques
        return True


# Casse et accentuation/
#Q1:
# EQUIV_NON_ACCENTUE={'à':'a','â':'a','é':'e','è':'e','ê':'e','ù':'u','ç':'c','î':'i'}
# #Q2:
def bas_casse_sans_accent(chaine:str)->str:
    """ renvoie lequivalent de chaine sans les majiscules et les caracteres identiques

    Précondition : 
    Exemple(s) :
    $$$ bas_casse_sans_accent('Orangé')
    'orange'
    $$$ bas_casse_sans_accent('la')
    'la'


    """

    res=''
    for elt in chaine:
        if elt in EQUIV_NON_ACCENTUE:
            res+= EQUIV_NON_ACCENTUE[elt]
        else:
            res+=elt.lower()
    return res
            
   
from lexique import LEXIQUE



#Anagrammes d'un mot : première méthode:

def anagrammes(mot: str) -> list[str]:
    """
    Renvoie la liste des mots figurant dans le LEXIQUE qui sont des anagrammes de mot.

    Précondition: aucune

    Exemples:

    $$$ anagrammes('orange')
    ['onagre', 'orange', 'orangé', 'organe', 'rongea']
    $$$ anagrammes('info')
    ['foin']
    $$$ anagrammes('Calbuth')
    []
    """
    with open('lexique.text','r') as f:
        mots=f.readlines()
        res=[]
        for elt in mots:
            if cle(mot)==cle(str(elt[:-1])):
                res.append(elt[:-1])
    return res


#Anagrammes d'un mot : seconde méthode:
#Q1: car les mots peuvent avoir des anagrammes multiples et déffirents. si on
#utilise les mots lexique comme clés , nous risquons de perdre des infoermations
#ou de creer des doublons dans notre dictionnaire

#Q2:



def sont_anagrammes(mot1:str,mot2:str)->bool:
    """renvoie True si les mots sont anagrammes 

    Précondition : 
    Exemple(s) :
    $$$ sont_anagrammes('Orangé', 'organE')
    True


    """
    mot1=bas_casse_sans_accent(mot1)
    mot2=bas_casse_sans_accent(mot2)
    return mot1==mot2d
    