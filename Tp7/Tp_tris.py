# l'expression qui permet de construire une liste des entiers
#de 0 àn-1 est range(n)


from random import shuffle
from typing import Callable
from compare import compare
from ap_decorators import count
from tris import *
import matplotlib.pyplot as plt
from anlyse_tris import tri_select
import timeit
from math import sqrt


def liste_alea(n:int)->list[int]:
    """construit une liste de longueur n contenant les entiers 0 à n-1 mélangés.

    Précondition : n>=0
    Exemple(s) :
    $$$ liste_alea(5)
    [3, 2, 0, 4, 1 ]
    $$$ liste_alea(3)
    [2, 1, 0]
    $$$ liste_alea(2)
    [0, 1]

    """
   
    L=list(range(n))
    random.shuffle(L)
    return L

#Representation graphique evec Matpotlib:

import matplotlib.pyplot as plt
abscisses = [1, 2, 4]
ordonnees = [2, 5, 0]
plt.plot(abscisses, ordonnees, color='blue')
plt.show()

#Évaluation expérimentale de la complexité en temps
compare = count(compare)

def analyser_tri(tri: Callable[[list[T], Callable[[T, T], int]], NoneType],
                 nbre_essais: int,
                 taille: int) -> float:
    """
    renvoie: le nombre moyen de comparaisons effectuées par l'algo tri
         pour trier des listes de taille t, la moyenne étant calculée
         sur n listes aléatoires.
    précondition: n > 0, t >= 0, la fonc
    """
    res = 0
    for i in range(nbre_essais):
        compare.counter = 0
        l = [k for k in range(taille)]
        shuffle(l)
        tri(l, compare)
        res += compare.counter
    return res / nbre_essais




#calcul de nombres moyens de compraisons pour des listes 

TAILLE_MAX = 100
L=[]
t=[]


for t in range(TAILLE_MAX+1):
    
    
    




