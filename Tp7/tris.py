#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:mod:`tris` module
:author: FIL - Faculté des Sciences et Technologies -
         Univ. Lille <http://portail.fil.univ-lille1.fr>_
:date: 2015, january
:dernière révision: février 2018


Tris de listes

- tri par sélection
- tri par insertion

"""

from types import NoneType
from typing import Callable, TypeVar
from compare import compare
T = TypeVar('T')

def est_trie(liste: list[T], comp: Callable[[T, T], int] = compare) -> bool:
    """
    renvoir True si et seulement si liste est triée selon l'ordre défini par comp

    précondition: les éléments de liste doivent être comparables selon comp

    exemples:

    $$$ est_trie([1, 2, 3, 4])
    True
    $$$ est_trie([1, 2, 4, 3])
    False
    $$$ est_trie([])
    True
    """
    i = 0
    res = True
    while res and i < len(liste) - 1:
        res = comp(liste[i], liste[i+1]) <= 0
        i += 1
    return res
    # ou plus simplement
    # return all(comp(liste[i], liste[i+1]) <= 0 for i in range(len(liste)-1))


################################################
#  TRI PAR SELECTION                           #
################################################

def echanger(liste: list[T], i: int, j: int) -> NoneType:
    """
    échange les éléments d'indice i et j de liste.

    précondition: 0 <= i,j < len(liste)

    exemples:

    $$$ l1 =  [3, 1, 4, 9, 5, 1, 2]
    $$$ l2 = l1.copy()
    $$$ echanger(l2, 3, 5)
    $$$ (l1[3], l1[5]) == (l2[5], l2[3])
    True
    """
    liste[i], liste[j] = liste[j], liste[i]


def select_min(liste: list[T], a: int, b: int, comp: Callable[[T, T], int]=compare) -> int:
    """
    renvoie l'indice du minimum dans la tranche liste[a:b]
    précondition: 0 <= a < b <= long(liste),
         éléments de liste comparables avec comp
    exemples:

    $$$ select_min([1, 2, 3, 4, 5, 6, 7, 0], 0, 8)
    7
    $$$ select_min([1, 2, 3, 4, 5, 6, 7, 0], 1, 7)
    1
    """
    ind_min = a
    # l'indice du plus petit élément de la tranche liste[a:a+1] est ind_min
    for i in range(a + 1, b):
        # supposons que l'indice du plus petit élément de la
        # tranche liste[a:i] est ind_min
        if comp(liste[i], liste[ind_min]) == -1:
            ind_min = i
        # alors l'indice du plus petit élément de la tranche liste[a:i+1]
        # est ind_min
    # à l'issue de l'itération l'indice du plus petit élément de la tranche
    # liste[a:b] est ind_min
    return ind_min


def tri_select(liste: list[T], comp: Callable[[T, T], int] = compare) -> NoneType:
    """
    modifie la liste liste en triant ses éléments selon l'ordre défini par comp
          Algorithme du tri par sélection du minimum
    précondition: liste liste homogène d'éléments comparables selon comp
    exemples:

    $$$ liste = [3, 1, 4, 1, 5, 9, 2]
    $$$ tri_select(liste)
    $$$ liste == [1, 1, 2, 3, 4, 5, 9]
    True
    $$$ from random import randrange
    $$$ l1 = [randrange(1000) for k in range(randrange(100))]
    $$$ l2 = l1.copy()
    $$$ tri_select(l2)
    $$$ est_trie(l2)
    True
    $$$ all(l1.count(elt) == l2.count(elt) for elt in l1)
    True
    $$$ all(l1.count(elt) == l2.count(elt) for elt in l2)
    True
    """
    n = len(liste)
    # la tranche liste[0:1] est triée
    for i in range(n - 1):
        # supposons la tranche liste[0:i+1] triée
        ind_min = select_min(liste, i, n, comp=comp)
        echanger(liste, i, ind_min)
        # alors la tranche liste[0:i+1] est triée
    # à l'issue de l'itération la tranche liste[0:n] est triée


################################################
#  TRI PAR INSERTION                           #
################################################

def inserer(liste: list[T], i: int, comp: Callable[[T, T], int] = compare) -> NoneType:
    """
    insère l'élément liste[i] à sa place dans la tranche
    liste[0:i+1] de sorte que cette tranche soit triée
    si liste[0:i] l'est auparavant

    précondition: 0 <= i < long(liste)
         éléments de liste comparables par comp
    exemples:

    $$$ liste = [1, 2, 4, 5, 3, 7, 6]
    $$$ inserer(liste, 4)
    $$$ liste == [1, 2, 3, 4, 5, 7, 6]
    True
    $$$ inserer(liste, 5)
    $$$ liste == [1, 2, 3, 4, 5, 7, 6]
    True
    $$$ inserer(liste, 6)
    $$$ liste == [1, 2, 3, 4, 5, 6, 7]
    True
    """
    aux = liste[i]
    k = i
    while k >= 1 and comp(aux, liste[k - 1]) == -1:
        liste[k] = liste[k - 1]
        k = k - 1
    liste[k] = aux


def tri_insert(liste: list[T], comp: Callable[[T, T], int] = compare) -> NoneType:
    """
    modifie la liste liste en triant ses éléments selon l'ordre défini par comp Algorithme du tri par insertion
    précondition: liste liste homogène d'éléments comparables selon comp
    exemples:

    $$$ liste = [3, 1, 4, 1, 5, 9, 2]
    $$$ tri_insert(liste)
    $$$ liste == [1, 1, 2, 3, 4, 5, 9]
    True
    $$$ from random import randrange
    $$$ l1 = [randrange(1000) for k in range(randrange(100))]
    $$$ l2 = l1.copy()
    $$$ tri_insert(l2)
    $$$ est_trie(l2)
    True
    $$$ all(l1.count(elt) == l2.count(elt) for elt in l1)
    True
    $$$ all(l1.count(elt) == l2.count(elt) for elt in l2)
    True
    """
    n = len(liste)
    # la tranche liste[0:1] est triée
    for i in range(1, n):
        # supposons la tranche liste[0:i] triée
        inserer(liste, i, comp=comp)
        # alors la tranche liste[0:i+1] est triée
    # à l'issue de l'itération la tranche liste[0:n] est triée


if (__name__ == '__main__'):
    import apl1test
    apl1test.testmod('tris.py')
